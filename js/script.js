
var swiperSteps= new Swiper('.swiper-steps', {
    loop: false,
    freeMode: true,
    spaceBetween: 10,
    slidesPerView: "auto",
    centeredSlides: false,
    breakpoints: {
    1300: {
        spaceBetween: 20,
      }
    }
  });


// PRICES TOGGLE 
var btnPlan = document.querySelectorAll('.btn-plan');
var planBlock = document.querySelectorAll('.prices__var');

btnPlan.forEach(function(btn) {
	btn.addEventListener("click", toggleActive);
});

function toggleActive () {
    var curBtn = this;
    var curBlock = curBtn.dataset.planName;
    if (!curBtn.matches('.active')) {
        for (var i = 0; i < btnPlan.length; ++i) {
            btnPlan[i].classList.remove('active');
         }
        curBtn.classList.add('active');
    }
    for (var i = 0; i < planBlock.length; ++i) {
        if (planBlock[i].classList.contains(curBlock)) {
            planBlock[i].classList.add('active');
        } else {
            planBlock[i].classList.remove('active');
        }
        
    }
}


// // faq accordion
// var accordion = new Accordion('.accordion-container',{
//     hideAll: false,
//     showAll: false,
//     showFirst: true,
//     panelId: 0
// });



// toggle mobile menu
var mobMenuToggle = document.getElementsByClassName('header__menu-toggle')[0];
var scrollLinks = document.getElementsByClassName('scroll-link');
var numScrollLinks = scrollLinks.length;

mobMenuToggle.addEventListener('click', function () {
    toggleMobileMenu(false);
});

function toggleMobileMenu(close) {
    if(!close) {
        mobMenuToggle.classList.toggle("open");
    } else {
        mobMenuToggle.classList.remove("open");
    }
}


// all anchor links
var lnks = document .querySelectorAll('.scroll-link');

for (var i = 0; i < lnks.length; i++) {
    lnks[i].onclick = function(e){
        e.preventDefault();
        var curLnk = this;

        var curLnkHash = curLnk.getAttribute("href").substring(1);
 
        var targSec = document.getElementById(curLnkHash);

        if(!isHidden(mobMenuToggle)) {
            toggleMobileMenu(true);
        }

        window.scrollTo({
            top: targSec.offsetTop,
            behavior: "smooth"
        });
    };
}


// check if element is visible - use to detect mobile view
function isHidden(el) {
  return (el.offsetParent === null)
}




// ANIMATIONS

//register the plugin (just once)
gsap.registerPlugin(MotionPathPlugin, ScrollTrigger); // register plugins


gsap.to("#line-dot", {
  scrollTrigger: {
    trigger: "#instructions",
    start: "top 50%-=200px",
    end: "bottom bottom",
    scrub: 0.2,
  },
  ease: "linear",
  motionPath:{
    path: "#line-path",
    align: "#line-path",
    autoRotate: true,
    alignOrigin: [0.5, 0.5]
  }
});

var cardsLightAnim = ScrollTrigger.create({
    trigger: "#instructions",
    start: "top 50%-=200px",
    end: "bottom bottom",
    scrub: 0.2,
    onUpdate: self => {
        //console.log("progress:", self.progress.toFixed(2), "direction:", self.direction);
        var progress = self.progress.toFixed(2);
        if (self.direction == 1) {
            if (progress > 0.01) {
                document.querySelector(".instructions__item-1").classList.add('active')
            } 
            if (progress > 0.25) {
                document.querySelector(".instructions__item-2").classList.add('active')
            } 
            if (progress > 0.43) {
                document.querySelector(".instructions__item-3").classList.add('active')
            } 
            if (progress > 0.75) {
                document.querySelector(".instructions__item-4").classList.add('active')
            } 
            if (progress > 0.93) {
                document.querySelector(".instructions__item-5").classList.add('active')
            }
        }
        if (self.direction == -1) {
            if (progress < 0.93) {
                document.querySelector(".instructions__item-5").classList.remove('active')
            }
            if (progress < 0.75) {
                document.querySelector(".instructions__item-4").classList.remove('active')
            } 
            if (progress < 0.45) {
                document.querySelector(".instructions__item-3").classList.remove('active')
            } 
            if (progress < 0.25) {
                document.querySelector(".instructions__item-2").classList.remove('active')
            } 
            if (progress < 0.01) {
                document.querySelector(".instructions__item-1").classList.remove('active')
            } 
        } 
    }
  });


// header background animate
var windowPos = window.pageYOffset;  
var pageHeader = document.querySelector('.s-header');

window.addEventListener('scroll', function() {
    windowPos = window.pageYOffset;

    if  (windowPos >= 10) {
        pageHeader.classList.add('active');
    } 
    if(windowPos < 10) {
        pageHeader.classList.remove('active');
    }
});


// TOOLTIPS
tippy('[data-tippy-content]', {
    trigger: 'mouseenter',
    appendTo: document.getElementsByClassName("wrapper")[0],
    interactive: false,
    arrow: false,
    maxWidth: 260,
    allowHTML: true
});


// COOKIE ALERT

function checkCookieAlert() {
    const isAgreed = localStorage.getItem('cookie_agreed');
    if ( !isAgreed ) {
        document.querySelector('.sec-cookie').style.display = null;
    }
}
function hideCookieAlert() {
    document.querySelector('.sec-cookie').style.display = 'none';
    localStorage.setItem('cookie_agreed', true);
}
document.querySelector('#cookie_agree').onclick = hideCookieAlert;
checkCookieAlert();
// END COOKIE ALERT